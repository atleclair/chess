//
//  Bishop.swift
//  chess
//
//  Created by Andrew Leclair on 12/18/18.
//  Copyright © 2018 Andrew Leclair. All rights reserved.
//

import Foundation
import UIKit

class Bishop: Piece {
    
    init(image: UIImage, team: Team) {
        self.image = image
        self.team = team
    }
    
    let value = 3
    let image: UIImage
    let team: Team
    weak var square: Board.Square?
    var legalMoveCoordinates: Set<Board.Coordinate> = Set<Board.Coordinate>()
    
    func CanMoveToSquare(toSquare: Board.Square, board: Board) -> Bool {
        return CanMoveToSquare(toSquare: toSquare, board: board, isTargetSquare: true)
    }
    
    private func CanMoveToSquare(toSquare: Board.Square, board: Board, isTargetSquare: Bool) -> Bool {

        guard let square = self.square, toSquare != square else {
            return false
        }
        
        let rise: Double = Double(abs(toSquare.coordinate.y - square.coordinate.y))
        let run: Double = Double(abs(toSquare.coordinate.x - square.coordinate.x))
        let slope = rise / run
        
        //If the slope from the current square to target isn't 1, then they aren't positioned diagonally
        guard slope == 1 else {
            return false
        }
        
        if !toSquare.isOccupied || (toSquare.piece!.team != square.piece!.team && isTargetSquare) {
            
            if rise == 1 && run == 1 {
                return true
            } else {
                //Not one square away, calculate direction and make recursive call
                let verticalDirection = square.coordinate.y - toSquare.coordinate.y
                let horizontalDirection = square.coordinate.x - toSquare.coordinate.x
                let horizontalStep = horizontalDirection < 0 ? -1 : 1
                let verticalStep = verticalDirection < 0 ? -1 : 1
                
                let nextSquareCoordinate = Board.Coordinate(x: toSquare.coordinate.x + horizontalStep, y: toSquare.coordinate.y + verticalStep)
                let nextToSquare = board.GetSquare(coord: nextSquareCoordinate)!
                
                return CanMoveToSquare(toSquare: nextToSquare, board: board, isTargetSquare: false)
            }
        }
        else {
            return false
        }
    }
    
    func MoveCompleted() {
        //      <#code#>
    }
}
