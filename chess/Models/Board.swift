//
//  Board.swift
//  chess
//
//  Created by Andrew Leclair on 12/18/18.
//  Copyright © 2018 Andrew Leclair. All rights reserved.
//

import Foundation
import UIKit

class Board {
    
    class Square: Equatable {
        
        init(highlightColor: UIColor, defaultColor: UIColor, coordinate: Board.Coordinate, piece: Piece? = nil) {
            self.highlightColor = highlightColor
            self.defaultColor = defaultColor
            self.coordinate = coordinate
            self.piece = piece
        }
        
        var highlightColor: UIColor?
        var isHighlighted: Bool = false
        var selectable: Bool = false
        let defaultColor: UIColor
        var coordinate: Board.Coordinate
        fileprivate(set) var piece: Piece?
        var isOccupied: Bool {
            get {
                return piece != nil
            }
        }
        
        static func == (lhs: Square, rhs: Square) -> Bool {
            return lhs.coordinate.x == rhs.coordinate.x && lhs.coordinate.y == rhs.coordinate.y
        }
    }
    
    var squares: [[Square]]
    
    struct Coordinate: Hashable {
        let x: Int
        let y: Int
        
        func hash(into: Coordinate) -> String {
            return "\(x)\(y)"
        }
    }
    
    init(withFormatter formatter: BoardFormatter) {
        squares = []
        for row in 0..<formatter.squareDimmension {
            var rowSquares: [Square] = []
            for colummn in 0..<formatter.squareDimmension {
                let coordinate = Board.Coordinate(x: colummn, y: row)
                let piece = formatter.piecePlacement[coordinate]
                let color1 = UIColor(red: 143/255, green: 153/255, blue: 62/255, alpha: 1.0)
                  let color2 = UIColor(red: 250/255, green: 200/255, blue: 160/255, alpha: 1.0)
                let squareColor = (row + colummn) % 2 == 0 ? color1 : color2
                let square = Square(highlightColor: UIColor.green, defaultColor: squareColor, coordinate: coordinate, piece: piece)
                piece?.square = square
                rowSquares += [square]
            }
            squares += [rowSquares]
        }
    }
    
    func GetSquare(coord: Coordinate) -> Square? {
        
        guard coord.y < squares.count, coord.y >= 0, coord.x < squares[coord.y].count, coord.x >= 0
            else {
                return nil
        }
        return squares[coord.y][coord.x]
    }
    
    func MovePiece(_ piece: Piece, toSquare square: Square) {
        piece.square?.piece = nil
        square.piece = piece
        piece.square = square
    }
}
