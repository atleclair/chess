//
//  BoardFormatter.swift
//  chess
//
//  Created by Daniel Leclair on 12/19/18.
//  Copyright © 2018 Andrew Leclair. All rights reserved.
//

import Foundation
typealias CoordinateRange = (Int, Int)

struct BoardFormatter {
        
    var squareDimmension: Int
    var piecePlacement: [Board.Coordinate : Piece] = [:]
    
    init(withSquareDimmension dimmension: Int, piecePlacement: [Board.Coordinate : Piece]) {
        self.squareDimmension = max(dimmension, 0)
        self.piecePlacement = piecePlacement
    }
}
