//
//  Knight.swift
//  chess
//
//  Created by Andrew Leclair on 12/18/18.
//  Copyright © 2018 Andrew Leclair. All rights reserved.
//

import Foundation
import UIKit

class Knight: Piece {
    
    let value = 3
    let image: UIImage
    let team: Team
    weak var square: Board.Square?
    var legalMoveCoordinates: Set<Board.Coordinate> = Set<Board.Coordinate>()
    
    init(image: UIImage, team: Team) {
        self.image = image
        self.team = team
    }
    
    func CanMoveToSquare(toSquare: Board.Square, board: Board) -> Bool {
        return CanMoveToSquare(toSquare: toSquare, board: board, isTargetSquare: true)
    }
    
    private func CanMoveToSquare(toSquare: Board.Square, board: Board, isTargetSquare: Bool) -> Bool {
        guard let square = self.square, toSquare != square else {
            return false
        }
        
        if (abs(toSquare.coordinate.x - square.coordinate.x) == 1 && abs (toSquare.coordinate.y - square.coordinate.y) == 2) ||  (abs(toSquare.coordinate.y - square.coordinate.y) == 1 && abs (toSquare.coordinate.x - square.coordinate.x) == 2) {
            if toSquare.piece?.team != self.team {
                return true
            } else {
                return false
            }
        }
        
        return false
    }
    
    func MoveCompleted() {
        
    }
}
