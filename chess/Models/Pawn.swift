//
//  Pawn.swift
//  chess
//
//  Created by Andrew Leclair on 12/18/18.
//  Copyright © 2018 Andrew Leclair. All rights reserved.
//

import Foundation
import UIKit

class Pawn: Piece {
    
    let value = 1
    let image: UIImage
    let team: Team
    var hasMoved = false
    weak var square: Board.Square?
    var legalMoveCoordinates: Set<Board.Coordinate> = Set<Board.Coordinate>()
    
    init(image: UIImage, team: Team) {
        self.image = image
        self.team = team
    }
    
    func CanMoveToSquare(toSquare: Board.Square, board: Board) -> Bool {
        
        guard let square = self.square else {
            return false
        }
        
        if !toSquare.isOccupied {
            if toSquare.coordinate.x != square.coordinate.x {
                return false
            }
            
            if hasMoved == false {
                if (toSquare.coordinate.y - square.coordinate.y) * team.teamBias == 2 {
                    return CanMoveToSquare(toSquare: board.GetSquare(coord: Board.Coordinate(x: toSquare.coordinate.x, y: toSquare.coordinate.y-1 * team.teamBias))!, board: board)
                }
            }
            
            if (toSquare.coordinate.y - square.coordinate.y) * team.teamBias == 1 {
                return true
            }
        }
        else { // the square is occupied
            if (toSquare.coordinate.y - square.coordinate.y) * team.teamBias == 1 && abs(toSquare.coordinate.x - square.coordinate.x) == 1 && toSquare.piece!.team != self.team { // move is diagonal, occupying piece is enemy
                return true
            }
            else { // square is not within legal movement
                return false
            }
        }
        return false
    }
    
    func MoveCompleted() {
        hasMoved = true
    }
}
