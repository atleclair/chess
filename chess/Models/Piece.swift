//
//  Piece.swift
//  chess
//
//  Created by Andrew Leclair on 12/18/18.
//  Copyright © 2018 Andrew Leclair. All rights reserved.
//

import Foundation
import UIKit

enum Team: String {
   case White
   case Black
   var teamBias: Int {
      get {
         switch self {
         case .White: return -1
         case .Black: return 1
         }
      }
   }
}

protocol Piece: class {
    
    func CanMoveToSquare(toSquare: Board.Square, board: Board) -> Bool
    func MoveCompleted()
    
    var value: Int { get }
    var image: UIImage { get }
    var team: Team { get }
    var legalMoveCoordinates: Set<Board.Coordinate> { get set }
    var square: Board.Square? { get set }
}
