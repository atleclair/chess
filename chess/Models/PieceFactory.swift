//
//  PieceFactory.swift
//  chess
//
//  Created by Daniel Leclair on 12/22/18.
//  Copyright © 2018 Andrew Leclair. All rights reserved.
//

import Foundation
import UIKit

enum PieceType {
    case Pawn
    case Rook
    case Bishop
    case Knight
    case Queen
    case King
}

class PieceFactory {
    
    private struct Constants {
        static let whitePawnImageName = "whitePawn"
        static let blackPawnImageName = "blackPawn"
        static let whiteRookImageName = "whiteRook"
        static let blackRookImageName = "blackRook"
        static let whiteBishopImageName = "whiteBishop"
        static let blackBishopImageName = "blackBishop"
        static let whiteKnightImageName = "whiteKnight"
        static let blackKnightImageName = "blackKnight"
        static let whiteQueenImageName = "whiteQueen"
        static let blackQueenImageName = "blackQueen"
        static let whiteKingImageName = "whiteKing"
        static let blackKingImageName = "blackKing"
    }
    
    static func Create(type: PieceType, team: Team) -> Piece {
        
        let image: UIImage
        
        switch type {
        case .Pawn:
            image = team == .White ? UIImage(named: Constants.whitePawnImageName)! : UIImage(named: Constants.blackPawnImageName)!
            return Pawn(image: image, team: team)
        case .Rook:
            image = team == .White ? UIImage(named: Constants.whiteRookImageName)! : UIImage(named: Constants.blackRookImageName)!
            return Rook(image: image, team: team)
        case .Bishop:
            image = team == .White ? UIImage(named: Constants.whiteBishopImageName)! : UIImage(named: Constants.blackBishopImageName)!
            return Bishop(image: image, team: team)
        case .Knight:
            image = team == .White ? UIImage(named: Constants.whiteKnightImageName)! : UIImage(named: Constants.blackKnightImageName)!
            return Knight(image: image, team: team)
        case .Queen:
            image = team == .White ? UIImage(named: Constants.whiteQueenImageName)! : UIImage(named: Constants.blackQueenImageName)!
            return Queen(image: image, team: team)
        case .King:
            image = team == .White ? UIImage(named: Constants.whiteKingImageName)! : UIImage(named: Constants.blackKingImageName)!
            return King(image: image, team: team)
        }
    }
}
