//
//  PiecePlacement.swift
//  chess
//
//  Created by Daniel Leclair on 12/27/18.
//  Copyright © 2018 Andrew Leclair. All rights reserved.
//

import Foundation

struct PiecePlacement {
    
    private(set) var boardCoordinates: [Board.Coordinate] = []
    var team: Team
    var pieceType: PieceType
    
    init(pieceType: PieceType, team: Team, coordinates: [(x: Int, y: Int)]) {
        
        self.pieceType = pieceType
        self.team = team
        
        for coord in coordinates {
            boardCoordinates += [Board.Coordinate(x: coord.x, y: coord.y)]
        }
    }
    
    init(pieceType: PieceType, team: Team, yCoordinate yCoord: Int, withXRange xRange: ClosedRange<Int>){
        
        self.pieceType = pieceType
        self.team = team
        
        for xCoord in xRange {
            boardCoordinates += [Board.Coordinate(x: xCoord, y: yCoord)]
        }
    }
    
    init(pieceType: PieceType, team: Team, xCoordinate xCoord: Int, withYRange yRange: ClosedRange<Int>){
        
        self.pieceType = pieceType
        self.team = team
        
        for yCoord in yRange {
            boardCoordinates += [Board.Coordinate(x: xCoord, y: yCoord)]
        }
    }
    
    
}
