//
//  PotentialMove.swift
//  chess
//
//  Created by Daniel Leclair on 12/22/18.
//  Copyright © 2018 Andrew Leclair. All rights reserved.
//

import Foundation

struct PotentialMoveContext {
    
    let legalMoveCoordinates: Set<Board.Coordinate>
    let pieceCoordinate: Board.Coordinate
    var finalMoveCoordinate: Board.Coordinate?
    
    init(legalMoveCoordinates: Set<Board.Coordinate>, pieceCoordinate: Board.Coordinate) {
        self.pieceCoordinate = pieceCoordinate
        self.legalMoveCoordinates = legalMoveCoordinates
    }
}
