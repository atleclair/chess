//
//  Queen.swift
//  chess
//
//  Created by Andrew Leclair on 12/18/18.
//  Copyright © 2018 Andrew Leclair. All rights reserved.
//

import Foundation
import UIKit

class Queen: Piece {
    
    var square: Board.Square?
    var value: Int = 9
    let image: UIImage
    let team: Team
    var legalMoveCoordinates: Set<Board.Coordinate> = Set<Board.Coordinate>()
    
    init(image: UIImage, team: Team) {
        self.image = image
        self.team = team
    }
    
    func CanMoveToSquare(toSquare: Board.Square, board: Board) -> Bool {
        return CanMoveToSquare(toSquare: toSquare, board: board, isTargetSquare: true)
    }
    
    private func CanMoveToSquare(toSquare: Board.Square, board: Board, isTargetSquare: Bool) -> Bool {
        guard let square = self.square, toSquare != square else {
            return false
        }

        let horizontalStep : Int
        let horizontalDifference = toSquare.coordinate.x - square.coordinate.x
        if horizontalDifference == 0 {
            horizontalStep = 0
        } else {
            horizontalStep = horizontalDifference < 0 ? 1 : -1
        }
        
        let verticalStep : Int
        let verticalDifference = toSquare.coordinate.y - square.coordinate.y
        if verticalDifference == 0 {
            verticalStep = 0
        } else {
            verticalStep = verticalDifference < 0 ? 1 : -1
        }

        if !toSquare.isOccupied || toSquare.piece!.team != self.team && isTargetSquare {
            
            if (toSquare.coordinate.x == square.coordinate.x) || (toSquare.coordinate.y == square.coordinate.y) {
                if abs(horizontalDifference) == 1 || abs(verticalDifference) == 1 {
                    return true
                } else { // toSquare is not adjacent, must walk back toward Rook
                    return CanMoveToSquare(toSquare: board.GetSquare(coord: Board.Coordinate(x: toSquare.coordinate.x + horizontalStep, y: toSquare.coordinate.y + verticalStep))!, board: board, isTargetSquare: false)
                }
            }
            
            let rise: Double = Double(abs(toSquare.coordinate.y - square.coordinate.y))
            let run: Double = Double(abs(toSquare.coordinate.x - square.coordinate.x))
            let slope = rise / run
            
            //If the slope from the current square to target isn't 1, then they aren't positioned diagonally
            guard slope == 1 else {
                return false
            }
            
            if rise == 1 && run == 1 {
                return true
            } else {
                //Not one square away, calculate direction and make recursive call
                let verticalDirection = square.coordinate.y - toSquare.coordinate.y
                let horizontalDirection = square.coordinate.x - toSquare.coordinate.x
                let horizontalStep = horizontalDirection < 0 ? -1 : 1
                let verticalStep = verticalDirection < 0 ? -1 : 1
                
                let nextSquareCoordinate = Board.Coordinate(x: toSquare.coordinate.x + horizontalStep, y: toSquare.coordinate.y + verticalStep)
                let nextToSquare = board.GetSquare(coord: nextSquareCoordinate)!
                
                return CanMoveToSquare(toSquare: nextToSquare, board: board, isTargetSquare: false)
            }
        }
        return false
    }
    
    func MoveCompleted() {
        
    }
}

