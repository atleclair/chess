//
//  Rook.swift
//  chess
//
//  Created by Andrew Leclair on 12/18/18.
//  Copyright © 2018 Andrew Leclair. All rights reserved.
//

import Foundation
import UIKit

class Rook: Piece {
    
    init(image: UIImage, team: Team) {
        self.image = image
        self.team = team
    }
    
    let value = 5
    let image: UIImage
    let team: Team
    weak var square: Board.Square?
    var legalMoveCoordinates: Set<Board.Coordinate> = Set<Board.Coordinate>()
 
    func CanMoveToSquare(toSquare: Board.Square, board: Board) -> Bool {
        return CanMoveToSquare(toSquare: toSquare, board: board, isTargetSquare: true)
    }
    
    private func CanMoveToSquare(toSquare: Board.Square, board: Board, isTargetSquare: Bool) -> Bool {
        guard let square = self.square, toSquare.coordinate.x == square.coordinate.x || toSquare.coordinate.y == square.coordinate.y, toSquare != square else {
            return false
        }
        
        let horizontalStep : Int
        let horizontalDifference = toSquare.coordinate.x - square.coordinate.x
        if horizontalDifference == 0 {
            horizontalStep = 0
        } else {
            horizontalStep = horizontalDifference < 0 ? 1 : -1
        }
        
        
        let verticalStep : Int
        let verticalDifference = toSquare.coordinate.y - square.coordinate.y
        if verticalDifference == 0 {
            verticalStep = 0
        } else {
            verticalStep = verticalDifference < 0 ? 1 : -1
        }
        
        // toSquare is adjacent
        
        if !toSquare.isOccupied || toSquare.piece!.team != self.team && isTargetSquare {
            if abs(horizontalDifference) == 1 || abs(verticalDifference) == 1 {
                return true
            } else { // toSquare is not adjacent, must walk back toward Rook
                return CanMoveToSquare(toSquare: board.GetSquare(coord: Board.Coordinate(x: toSquare.coordinate.x + horizontalStep, y: toSquare.coordinate.y + verticalStep))!, board: board, isTargetSquare: false)
            }
        }
        return false
    }
    
    func MoveCompleted() {}
}

