//
//  AudioManager.swift
//  chess
//
//  Created by Andrew Leclair on 12/26/18.
//  Copyright © 2018 Andrew Leclair. All rights reserved.
//

import Foundation
import AVKit

class AudioManager {
    static let sharedInstance = AudioManager()
    enum Sound: String {
        case PickUpSound = "click"
        case PutDownSound = "pickUpSound"
        case Capture = "putDownSound"
    }
    var audioPlayer : AVAudioPlayer?
    
    init() {}
    
    func PlaySound(_ sound: AudioManager.Sound) {
            guard let URLPath = Bundle.main.path(forResource: sound.rawValue, ofType: ".mp3") else {
                return
            }
            let url = URL(fileURLWithPath: URLPath)
            audioPlayer = try? AVAudioPlayer(contentsOf: url)
            
            audioPlayer?.prepareToPlay()
            audioPlayer?.play()
    }
}
