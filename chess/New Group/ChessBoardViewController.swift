//
//  ViewController.swift
//  chess
//
//  Created by Andrew Leclair on 12/17/18.
//  Copyright © 2018 Andrew Leclair. All rights reserved.
//

import UIKit

class ChessBoardViewController: UIViewController, UISquareInteraction, GameStateDelegate {

    @IBOutlet weak var boardRowsStack: UIStackView!
  
    var uiboard: UIBoard?
    var gameManager: GameManager?
    
    //Contains the context of a move in progress before it is finalized by the player selecting the target square
   var potentialMove: PotentialMoveContext? {
      willSet {
         if let potentialMove = potentialMove {
            gameManager?.RemoveHighlightSquares(withCoordinates: potentialMove.legalMoveCoordinates)
         }
      }
   }
    
    override func viewDidLoad() {
      super.viewDidLoad()
        gameManager = GameManager()
        gameManager?.delegate = self
        createUIBoard(withBoard: gameManager!.board!)
    }
    
    private func createUIBoard(withBoard board: Board) {
        
        uiboard = UIBoard()
        boardRowsStack.addArrangedSubview(UIView())
        
        for row in 0..<board.squares.count {

            let rowStack = UIStackView()
            rowStack.axis = .horizontal
            rowStack.distribution = .fillEqually
            rowStack.translatesAutoresizingMaskIntoConstraints = false

            for square in board.squares[row] {
                
                let uiSquare = UISquare(withSquare: square)
                uiboard!.uiSquares[square.coordinate] = uiSquare
                uiSquare.delegate = self
                rowStack.addArrangedSubview(uiSquare)
            }

            boardRowsStack.addArrangedSubview(rowStack)
            rowStack.widthAnchor.constraint(equalTo: boardRowsStack.widthAnchor).isActive = true
        }
    }
    
    func didTapSquare(atCoordinate coordinate: Board.Coordinate) {
        
        guard let gameManager = gameManager, let board = gameManager.board, let tappedSquare = board.GetSquare(coord: coordinate) else {
            return
        }
        
        //If potential move has a value, this tap is to select a square for movement
        if let potentialMove = potentialMove, potentialMove.legalMoveCoordinates.contains(tappedSquare.coordinate) {

            if let square = board.GetSquare(coord: potentialMove.pieceCoordinate), let piece = square.piece  {
                gameManager.MovePiece(piece, toSquare: tappedSquare)

            }
            
            self.potentialMove = nil
            tappedSquare.piece?.MoveCompleted()
            gameManager.EndTurn()
            switch gameManager.currentTeam {
            case .White:
                self.view.backgroundColor = UIColor.white
            case .Black:
                self.view.backgroundColor = UIColor.black
            }
        }
        //Otherwise, when potential move is nil or outside of the legal options, check if this is a new piece selection
        //Get the square model based on coordinate from tapped UISquare, and then get piece from square if there is one
        else if let piece = tappedSquare.piece {
            
            //Clear any current potential move
            self.potentialMove = nil
            
            //Check to make sure piece belongs to current team
            if piece.team == gameManager.currentTeam {
                AudioManager.sharedInstance.PlaySound(.PickUpSound)
                let legalMoves = gameManager.GetAllLegalMoves(forPiece: piece)
                gameManager.HighlightSquares(withCoordinates: legalMoves)
                potentialMove = PotentialMoveContext(legalMoveCoordinates: legalMoves, pieceCoordinate: coordinate)
            }
        } else {
         self.potentialMove = nil
      }
        
        uiboard?.Reload(withBoard: board)
    }
    
    func GameStateEntered(_ gameState: GameState) {
        
        let label = UILabel()
        label.font = UIFont.preferredFont(forTextStyle: .title1)
        label.textColor = UIColor.white
        label.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(label)
        NSLayoutConstraint.activate([
            label.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
            label.centerYAnchor.constraint(equalTo: self.view.centerYAnchor)
        ])
        
        switch gameState{
        case .Check(_):
            label.text = "Check"
            
            UIView.animate(withDuration: 1, animations: {
            label.alpha = 0
            }) { _ in
                label.removeFromSuperview()
            }
            
        case .Checkmate(let team):
            label.text = "\(team.rawValue) Loses!"
        }
    }
}

