//
//  TurnManager.swift
//  chess
//
//  Created by Daniel Leclair on 12/21/18.
//  Copyright © 2018 Andrew Leclair. All rights reserved.
//

import Foundation

protocol GameStateDelegate: class{
    func GameStateEntered(_ gameState: GameState)
}

enum GameState {
    case Check(team: Team)
    case Checkmate(team: Team)
}

class GameManager {
    
    var turnCount = 0
    var currentTeam: Team = .White
    var board: Board?
    var remainingPieces: [Team : [Piece]] = [:]
    var teamInCheck: Team?
    var delegate: GameStateDelegate?
    
    static let standardPlacement: [PiecePlacement] = [
        PiecePlacement(pieceType: .Pawn, team: .Black, yCoordinate: 1, withXRange: 0...7),
        PiecePlacement(pieceType: .Rook, team: .Black, coordinates: [(0, 0), (7, 0)]),
        PiecePlacement(pieceType: .Bishop, team: .Black, coordinates: [(2, 0), (5, 0)]),
        PiecePlacement(pieceType: .Knight, team: .Black, coordinates: [(1, 0), (6, 0)]),
        PiecePlacement(pieceType: .Queen, team: .Black, coordinates: [(3, 0)]),
        PiecePlacement(pieceType: .King, team: .Black, coordinates: [(4, 0)]),
        PiecePlacement(pieceType: .Pawn, team: .White, yCoordinate: 6, withXRange: 0...7),
        PiecePlacement(pieceType: .Rook, team: .White, coordinates: [(0, 7), (7, 7)]),
        PiecePlacement(pieceType: .Bishop, team: .White, coordinates: [(2, 7), (5, 7)]),
        PiecePlacement(pieceType: .Knight, team: .White, coordinates: [(1, 7), (6, 7)]),
        PiecePlacement(pieceType: .Queen, team: .White, coordinates: [(3, 7)]),
        PiecePlacement(pieceType: .King, team: .White, coordinates: [(4, 7)])
    ]
    
    init() {
        StartGame()
    }
    
    func EndTurn() {
        
        turnCount += 1
        
        switch currentTeam {
        case .White:
            currentTeam = .Black
        case .Black:
            currentTeam = .White
        }
        
        ComputeAllMoves()
        
        teamInCheck = IsInCheck(team: currentTeam) ? currentTeam : nil
        if let teamInCheck = teamInCheck, let pieces = remainingPieces[teamInCheck] {
            for piece in pieces {
                if GetAllLegalMoves(forPiece: piece).count > 0 {
                    delegate?.GameStateEntered(.Check(team: currentTeam))
                    return
                }
            }
            
            //There are no remaining moves, end the game
            CompleteGame()
        }
        
    }
    
    func CompleteGame() {
        delegate?.GameStateEntered(.Checkmate(team: teamInCheck!))
        currentTeam = .White
        turnCount = 0
    }
    
    func StartGame() {
        board = Board(withFormatter: GenerateBoardFormatter())
        ComputeAllMoves()
    }
    
    private func GenerateBoardFormatter() -> BoardFormatter {
        
        //Generate board formatter
        var piecePlacement: [Board.Coordinate : Piece] = [:]
        
        for placement in GameManager.standardPlacement {
            for coord in placement.boardCoordinates {
                let piece = PieceFactory.Create(type: placement.pieceType, team: placement.team)
                
                //Add new piece to remaining pieces
                if remainingPieces[placement.team] == nil {
                    remainingPieces[placement.team] = [piece]
                } else {
                    remainingPieces[placement.team]! += [piece]
                }
                
                piecePlacement[coord] = piece
            }
        }
        
        return BoardFormatter(withSquareDimmension: 8, piecePlacement: piecePlacement)
    }
    
    //Compute moves using piece model's CanMoveToSquare method. This does not take into account special games rules such
    //as check, only collisions with other pieces
    private func ComputeAllMoves() {
        
        guard let board = board else {
            return
        }
        
        for teamPieces in remainingPieces.values {
            for piece in teamPieces {
                var legalMoveCoordinates: Set<Board.Coordinate> = Set<Board.Coordinate>()
                //Iterate through each square model in the board and set it to be highlighted if the piece can move there
                for row in 0..<board.squares.count {
                    for square in board.squares[row] {
                        if piece.CanMoveToSquare(toSquare: square, board: board) {
                            legalMoveCoordinates.insert(square.coordinate)
                        }
                    }
                }
                
                piece.legalMoveCoordinates = legalMoveCoordinates
            }
        }
    }
    
    func MovePiece(_ piece: Piece, toSquare: Board.Square) {
 
        guard let board = board else {
            return
        }
        
        if let takenPiece = toSquare.piece {
            AudioManager.sharedInstance.PlaySound(.Capture)
            remainingPieces[takenPiece.team] = remainingPieces[takenPiece.team]?.filter { piece in                
                return piece !== takenPiece
            }
        } else {
            AudioManager.sharedInstance.PlaySound(.PutDownSound)
        }
        
        board.MovePiece(piece, toSquare: toSquare)
    }
    
    //Take into account board state and rules
    func GetAllLegalMoves(forPiece piece: Piece) -> Set<Board.Coordinate> {
        
        guard let oldLocation = piece.square, let board = board else {
            return Set<Board.Coordinate>()
        }
        
        var safeMovementSet = piece.legalMoveCoordinates
        
        for move in piece.legalMoveCoordinates {
            if let newSquare = board.GetSquare(coord: move) {
                
                let potentialPiece = newSquare.piece
                board.MovePiece(piece, toSquare: newSquare)
                ComputeAllMoves()
                if IsInCheck(team: currentTeam) {
                    safeMovementSet.remove(move)
                }
                board.MovePiece(piece, toSquare: oldLocation)
                
                if let potentialPiece = potentialPiece {
                  board.MovePiece(potentialPiece, toSquare: newSquare)
                }
            }
        }
        
        //Revert to original computation
        ComputeAllMoves()
        return safeMovementSet
    }
    
    func IsInCheck(team: Team) -> Bool {
        
        guard let pieces = remainingPieces[team], let king = pieces.first(where: { $0 is King}) as? King, let kingCoordinate = king.square?.coordinate else {
            return false
        }
        
        //Check the possible moves of other team's pieces
        for otherTeam in remainingPieces.keys {
            
            //Skip the team in question
            if otherTeam == team {
                continue
            }
            
            if let pieces = remainingPieces[otherTeam] {
                
                //If any team can move a piece onto the king's square, it is in check
                for piece in pieces {
                        if piece.legalMoveCoordinates.contains(kingCoordinate) {
                            return true
                        }
                    }
                }
            }
        
        return false
    }
    
    func HighlightSquares(withCoordinates coordinates: Set<Board.Coordinate>) {
        for coordinate in coordinates {
            board?.GetSquare(coord: coordinate)?.isHighlighted = true
        }
    }
    
    func RemoveHighlightSquares(withCoordinates coordinates: Set<Board.Coordinate>) {
        for coordinate in coordinates {
            board?.GetSquare(coord: coordinate)?.isHighlighted = false
        }
    }
}
