//
//  UIBoard.swift
//  chess
//
//  Created by Daniel Leclair on 12/21/18.
//  Copyright © 2018 Andrew Leclair. All rights reserved.
//

import Foundation
import UIKit

class UIBoard {
    var uiSquares: [Board.Coordinate : UISquare] = [:]
    
    func Reload(withBoard board: Board) {
        //Iterate through the board model, find each UISquare for the corresponding model and reload it
        for row in 0..<board.squares.count {
            for square in board.squares[row] {
                if let uiSquare = uiSquares[square.coordinate] {
                    uiSquare.reload(withSquare: square)
                }
            }
        }
    }
}
