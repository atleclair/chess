//
//  UISquare.swift
//  chess
//
//  Created by Andrew Leclair on 12/17/18.
//  Copyright © 2018 Andrew Leclair. All rights reserved.
//

import Foundation
import UIKit

protocol UISquareInteraction: class {
    func didTapSquare(atCoordinate coordinate: Board.Coordinate)
}

class UISquare : UIView {
    
    public weak var delegate: UISquareInteraction?
    
    private struct Constants {
        static let imagePadding: CGFloat = 8
    }
    
    private var pieceImageView : UIImageView?
    private var coordinate: Board.Coordinate?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder : aDecoder)
    }
    
    init(withSquare square: Board.Square) {
        super.init(frame: CGRect())
        
        self.pieceImageView = UIImageView()
        self.pieceImageView!.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(pieceImageView!)
        
        //Set the view representation to match the model
        reload(withSquare: square)
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(squareTapped))
        self.addGestureRecognizer(tapGestureRecognizer)
        
        NSLayoutConstraint.activate([
            pieceImageView!.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: Constants.imagePadding),
            pieceImageView!.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -Constants.imagePadding),
            pieceImageView!.topAnchor.constraint(equalTo: self.topAnchor, constant: Constants.imagePadding),
            pieceImageView!.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -Constants.imagePadding)
            ])
    }
    
    func reload(withSquare square: Board.Square) {

        //Sets the view representation to match the model
        self.pieceImageView!.backgroundColor = UIColor.clear
        
        if let highlightColor = square.highlightColor, square.isHighlighted {
             self.backgroundColor = highlightColor
        } else
        {
            self.backgroundColor = square.defaultColor
        }
        
        self.coordinate = square.coordinate
        self.pieceImageView!.image = square.piece?.image
    }
    
    @objc private func squareTapped() {
        
        if let coordinate = self.coordinate {
            delegate?.didTapSquare(atCoordinate: coordinate)
        }
    }
}

